using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using bancaportella_api.Service;
using bancaportella_api.Infra;
using bancaportella_api.Domain;

namespace bancaportella_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        IProdutoService _service;

        public ProdutoController(IProdutoService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<List<Produtos>>> Get()
        {
            try{
                return StatusCode(200, await _service.GetList());
            }
            catch(Exception ex){
                return StatusCode(500, ex.Message);
            }            
        }

        [Route("{Produto_Id}")]
        [HttpGet]
        public async Task<ActionResult<Produtos>> GetItem(long Produto_Id)
        {
            try{
                return StatusCode(200, await _service.GetItem(Produto_Id));
            }
            catch(Exception ex){
                return StatusCode(500, ex.Message);
            }            
        }

        [Route("{Usuario_Id}")]
        [HttpPost]
        public async Task<ActionResult<Produtos>> Post([FromRoute] long Usuario_Id, [FromBody] ProdutoDomain model)
        {
            try{
                return StatusCode(200, await _service.InsertUpdate(Usuario_Id, model));
            }
            catch(Exception ex){
                return StatusCode(500, ex.Message);
            }            
        }
    }
}
