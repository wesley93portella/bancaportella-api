using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using bancaportella_api.Service;
using bancaportella_api.Security;
using Microsoft.AspNetCore.Authorization;
using bancaportella_api.Domain;

namespace bancaportella_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : ControllerBase
    {
        IUsuarioService _service;

        public UsuarioController(IUsuarioService service)
        {
            _service = service;
        }

        [Route("Authenticate")]
        [HttpPost]
        public async Task<ActionResult<UsuarioDomain>> Authenticate([FromBody] UsuarioDomain usuario)
        {
            try{
                return StatusCode(200, await _service.Authenticate(usuario.Email, usuario.Senha));
            }
            catch(Exception ex){
                return StatusCode(500, new {ex.Message});
            }
        }
    }
}
