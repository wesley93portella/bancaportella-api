﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bancaportella_api.Domain
{
    public class ProdutoDomain
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public string Categoria { get; set; }
        public decimal Preco { get; set; }
        public DateTime DataAtualizacao { get; set; }
        public long UsuarioAtualizacaoId { get; set; }
    }
}
