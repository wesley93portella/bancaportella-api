﻿using System;
using System.Collections.Generic;

namespace bancaportella_api.Infra
{
    public partial class Produtos
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public string Categoria { get; set; }
        public decimal Preco { get; set; }
        public DateTime DataAtualizacao { get; set; }
        public long UsuarioAtualizacaoId { get; set; }

        public virtual Usuarios UsuarioAtualizacao { get; set; }
    }
}
