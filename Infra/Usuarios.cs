﻿using System;
using System.Collections.Generic;

namespace bancaportella_api.Infra
{
    public partial class Usuarios
    {
        public Usuarios()
        {
            Produtos = new HashSet<Produtos>();
        }

        public long Id { get; set; }
        public string NomeUsuario { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }

        public virtual ICollection<Produtos> Produtos { get; set; }
    }
}
