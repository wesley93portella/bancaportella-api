# README #

Crição do Banco de Dados 

CREATE DATABASE bancaportella

CREATE TABLE dbo.Usuarios (
  Id bigint NOT NULL,
  NomeUsuario varchar(50) NOT NULL,
  Email varchar(50) NOT NULL,
  Senha int NOT NULL,
  PRIMARY KEY CLUSTERED (Id)
)
GO

CREATE TABLE dbo.Produtos (
  Id bigint NOT NULL,
  Nome varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  Categoria varchar(8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
  Preco money NOT NULL,
  DataAtualizacao datetime NOT NULL,
  UsuarioAtualizacao_Id bigint NOT NULL,
  CONSTRAINT Produtos_ck CHECK ([Categoria]='alimento' OR [Categoria]='bebida' OR [Categoria]='outros'),
  CONSTRAINT Produtos_fk FOREIGN KEY (UsuarioAtualizacao_Id) 
  REFERENCES dbo.Usuarios (Id) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
)
ON [PRIMARY]
GO
