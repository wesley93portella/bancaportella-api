﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using bancaportella_api.Infra;
using bancaportella_api.Domain;
using AutoMapper;

namespace bancaportella_api.Service
{
    public interface IProdutoService
    {
        Task<ProdutoDomain> GetItem(long Produto_Id);
        Task<List<ProdutoDomain>> GetList();
        Task<bool> InsertUpdate(long Usuario_Id, ProdutoDomain model);
    }

    public class ProdutoService : IProdutoService
    {
        private readonly dbContext _dbContext;
        IMapper _mapper;

        public ProdutoService(dbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<ProdutoDomain> GetItem(long Produto_Id)
        {
            try
            {
                ProdutoDomain produtoDomain = new ProdutoDomain();
                Produtos produto = await _dbContext.Produtos.Where(x => x.Id == Produto_Id).FirstOrDefaultAsync();

                if (produto != null)
                    produtoDomain = _mapper.Map<ProdutoDomain>(produto);
                else
                    throw new ArgumentException("Produto não encontrado");

                return produtoDomain;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        public async Task<List<ProdutoDomain>> GetList()
        {
            try
            {
                List<ProdutoDomain> produtosDomain = new List<ProdutoDomain>();
                List<Produtos> produtos = await _dbContext.Produtos.ToListAsync();
                if (produtos.Count > 0)
                    produtosDomain = _mapper.Map<List<ProdutoDomain>>(produtos);

                return produtosDomain;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        public async Task<bool> InsertUpdate(long Usuario_Id, ProdutoDomain model)
        {
            try
            {
                Produtos produto = new Produtos();
                if (model != null)
                    produto = _mapper.Map<Produtos>(model);
                //Busca o produto no banco
                Produtos model_db = await _dbContext.Produtos.Where(x => x.Id == model.Id).AsNoTracking().FirstOrDefaultAsync();
                //Guarda quem esta atualizado o registro e quando esta sendo feito
                model.UsuarioAtualizacaoId = Usuario_Id;
                model.DataAtualizacao = DateTime.Now;
                //Valida os Campos no banco
                ValidaCampos(produto);
                //Se não encontrar o produto é inseriodo caso contrario ele é atualizado
                if (model_db == null) 
                {
                    _dbContext.Produtos.Add(produto);
                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    _dbContext.Entry(produto).State = EntityState.Modified;
                    await _dbContext.SaveChangesAsync();
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
        }

        public void ValidaCampos(Produtos model)
        {
            if(model.Categoria.Length == 0)
                throw new ArgumentException("Preencha Todos os Campos para Continuar");
            if (model.Nome.Length == 0)
                throw new ArgumentException("Preencha Todos os Campos para Continuar");
        }
    }
}