﻿using System.Collections.Generic;
using System;
using System.Reflection;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using bancaportella_api.Domain;
using bancaportella_api.Infra;

namespace bancaportella_api.Service
{
    public interface IUsuarioService
    {
        Task<UsuarioDomain> Authenticate(string login, string password);
    }

    public class UsuarioService : IUsuarioService
    {
        private readonly dbContext _dbContext;

        public UsuarioService(dbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<UsuarioDomain> Authenticate(string email, string senha)
        {

            var userLogin = await _dbContext.Usuarios.Where(u => u.Email == email && u.Senha == senha).FirstOrDefaultAsync();

            if (userLogin == null)
                throw new ArgumentException("Usuário/Senha não encontrado!");

            try
            {
                _dbContext.Entry(userLogin).State = EntityState.Modified;
                await _dbContext.SaveChangesAsync();

                return new UsuarioDomain()
                {
                    Id = userLogin.Id,
                    Email = userLogin.Email,
                    NomeUsuario = userLogin.NomeUsuario,
                };
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.InnerException.Message);
            }
        }
    }
}